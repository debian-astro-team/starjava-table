From: Ole Streicher <olebole@debian.org>
Date: Mon, 20 Feb 2017 11:01:05 +0100
Subject: Exclude some tests

FormatsTest and StorageTest depend on fits and votable, which shall
not be build-deps to avoid circular dependencies. So, during build we can't
test them. However, for CI tests we have everything available, and they
will be included in the test.

The FormatsTest needs to be adjusted to the supported formats.
---
 build.xml                                          | 11 ++++++
 .../uk/ac/starlink/table/FormatsTest.java          | 46 ++--------------------
 src/testcases/uk/ac/starlink/table/SchemeTest.java |  2 +-
 3 files changed, 15 insertions(+), 44 deletions(-)

diff --git a/build.xml b/build.xml
index 6259dbe..4962b0d 100644
--- a/build.xml
+++ b/build.xml
@@ -316,6 +316,11 @@
                classname="uk.ac.starlink.util.DataSource"/>
     <fail message="No Util available" unless="util.present"/>
 
+    <!-- Need Votable and Fits for some testcases, not essential -->
+    <available property="votable.present"
+               classpathref="classpath"
+               classname="uk.ac.starlink.votable.VOTableWriter"/>
+
     <!-- Need JUnit for testcases, not essential -->
     <available property="junit.present"
                classpathref="classpath"
@@ -935,6 +940,9 @@
 
       <classpath refid="tests-classpath"/>
       <exclude name="uk/ac/starlink/table/join/SkyPixellatorTest.java"/>
+      <exclude name="uk/ac/starlink/table/FormatsTest.java" unless="votable.present"/>
+      <exclude name="uk/ac/starlink/table/HealpixTest.java" unless="votable.present"/>
+      <exclude name="uk/ac/starlink/table/storage/StorageTest.java" unless="votable.present"/>
 
     </javac>
 
@@ -973,6 +981,9 @@
         <fileset dir="${tests.dir}">
           <include name="**/*Test*"/>
           <exclude name="uk/ac/starlink/table/join/SkyPixellatorTest.java"/>
+          <exclude name="uk/ac/starlink/table/FormatsTest.java" unless="votable.present"/>
+          <exclude name="uk/ac/starlink/table/HealpixTest.java" unless="votable.present"/>
+          <exclude name="uk/ac/starlink/table/storage/StorageTest.java" unless="votable.present"/>
         </fileset>
       </batchtest>
 
diff --git a/src/testcases/uk/ac/starlink/table/FormatsTest.java b/src/testcases/uk/ac/starlink/table/FormatsTest.java
index 4ef3749..308ac6a 100644
--- a/src/testcases/uk/ac/starlink/table/FormatsTest.java
+++ b/src/testcases/uk/ac/starlink/table/FormatsTest.java
@@ -23,8 +23,6 @@ import org.xml.sax.InputSource;
 import org.xml.sax.SAXException;
 import uk.ac.starlink.ecsv.EcsvTableBuilder;
 import uk.ac.starlink.ecsv.EcsvTableWriter;
-import uk.ac.starlink.feather.FeatherTableBuilder;
-import uk.ac.starlink.feather.FeatherTableWriter;
 import uk.ac.starlink.fits.AbstractFitsTableWriter;
 import uk.ac.starlink.fits.AbstractWideFits;
 import uk.ac.starlink.fits.BintableStarTable;
@@ -33,8 +31,6 @@ import uk.ac.starlink.fits.FitsTableBuilder;
 import uk.ac.starlink.fits.FitsTableWriter;
 import uk.ac.starlink.fits.HealpixFitsTableWriter;
 import uk.ac.starlink.fits.WideFits;
-import uk.ac.starlink.parquet.ParquetTableBuilder;
-import uk.ac.starlink.parquet.ParquetTableWriter;
 import uk.ac.starlink.table.storage.AdaptiveByteStore;
 import uk.ac.starlink.table.storage.ByteStoreRowStore;
 import uk.ac.starlink.table.storage.FileByteStore;
@@ -97,9 +93,7 @@ public class FormatsTest extends TableCase {
         LogUtils.getLogger( "uk.ac.starlink.table" ).setLevel( Level.WARNING );
         LogUtils.getLogger( "uk.ac.starlink.fits" ).setLevel( Level.SEVERE );
         LogUtils.getLogger( "uk.ac.starlink.votable" ).setLevel( Level.WARNING);
-        LogUtils.getLogger( "uk.ac.starlink.feather" ).setLevel( Level.SEVERE );
         LogUtils.getLogger( "uk.ac.starlink.ecsv" ).setLevel( Level.SEVERE );
-        LogUtils.getLogger( "uk.ac.starlink.parquet" ).setLevel( Level.WARNING);
     }
 
     private StarTable table;
@@ -201,8 +195,8 @@ public class FormatsTest extends TableCase {
         String[] fnames = new String[] {
             "fits-basic", "fits-plus", "fits-var", "fits-healpix",
             "colfits-basic", "colfits-plus",
-            "votable", "ecsv", "feather", "text", "ascii", "csv",
-            "ipac", "tst", "html", "latex", "mirage",
+            "votable", "ecsv", "text", "ascii", "csv",
+            "ipac", "tst", "html", "latex",
         };
         for ( String fname : fnames ) {
             assertNotNull( tout.getHandler( fname ) );
@@ -212,7 +206,7 @@ public class FormatsTest extends TableCase {
         StarTableFactory tfact = new StarTableFactory();
         String[] bnames = new String[] {
             "fits", "colfits-basic", "colfits-plus",
-            "votable", "cdf", "ecsv", "parquet", "feather",
+            "votable", "cdf", "ecsv", 
         };
         for ( String bname : bnames ) {
             assertNotNull( tfact.getTableBuilder( bname ) );
@@ -276,11 +270,7 @@ public class FormatsTest extends TableCase {
             "VOTable",
             "CDF",
             "ECSV",
-            "PDS4",
             "MRT",
-            "parquet",
-            "feather",
-            "GBIN",
         };
         String[] knownFormats = new String[] {
             "FITS-plus",
@@ -290,16 +280,11 @@ public class FormatsTest extends TableCase {
             "VOTable",
             "CDF",
             "ECSV",
-            "PDS4",
             "MRT",
-            "parquet",
-            "feather",
-            "GBIN",
             "ASCII",
             "CSV",
             "TST",
             "IPAC",
-            "HAPI",
             "WDC",
         };
         StarTableFactory factory = new StarTableFactory();
@@ -343,8 +328,6 @@ public class FormatsTest extends TableCase {
             "fits-healpix",
             "votable",
             "ecsv",
-            "parquet",
-            "feather",
             "text",
             "ascii",
             "csv",
@@ -352,7 +335,6 @@ public class FormatsTest extends TableCase {
             "tst",
             "html",
             "latex",
-            "mirage",
         };
         String[] gotFormats = new String[ handlers.size() ];
         for ( int j = 0; j < handlers.size(); j++ ) {
@@ -558,22 +540,6 @@ public class FormatsTest extends TableCase {
                            new EcsvTableBuilder(), "ecsv" );
         exerciseReadWrite( EcsvTableWriter.COMMA_WRITER,
                            new EcsvTableBuilder(), "ecsv" );
-
-        ParquetTableBuilder parquetBuilder = new ParquetTableBuilder();
-        ParquetTableWriter parquetWriter = new ParquetTableWriter();
-        parquetBuilder.setCacheCols( Boolean.FALSE );
-        parquetWriter.setGroupArray( false );
-        exerciseReadWrite( parquetWriter, parquetBuilder, "parquet");
-        parquetBuilder.setCacheCols( Boolean.TRUE );
-        parquetWriter.setGroupArray( true );
-        exerciseReadWrite( parquetWriter, parquetBuilder, "parquet");
-
-        exerciseReadWrite(
-            new FeatherTableWriter( false, StoragePolicy.PREFER_MEMORY ),
-            new FeatherTableBuilder(), "feather" );
-        exerciseReadWrite(
-            new FeatherTableWriter( true, StoragePolicy.PREFER_MEMORY ),
-            new FeatherTableBuilder(), "feather" );
         exerciseReadWrite( new AsciiTableWriter(),
                            new AsciiTableBuilder(), "text" );
         exerciseReadWrite( new CsvTableWriter( true ),
@@ -651,12 +617,6 @@ public class FormatsTest extends TableCase {
         else if ( "ecsv".equals( equalMethod ) ) {
             assertEcsvTableEquals( t1, t2 );
         }
-        else if ( "parquet".equals( equalMethod ) ) {
-            assertParquetTableEquals( t1, t2 );
-        }
-        else if ( "feather".equals( equalMethod ) ) {
-            assertFeatherTableEquals( t1, t2 );
-        }
         else if ( "exact".equals( equalMethod ) ) {
             assertTableEquals( t1, t2 );
         }
diff --git a/src/testcases/uk/ac/starlink/table/SchemeTest.java b/src/testcases/uk/ac/starlink/table/SchemeTest.java
index 03c8f40..0b55164 100644
--- a/src/testcases/uk/ac/starlink/table/SchemeTest.java
+++ b/src/testcases/uk/ac/starlink/table/SchemeTest.java
@@ -13,7 +13,7 @@ public class SchemeTest extends TestCase {
         Map<String,TableScheme> schemes = tfact.getSchemes();
         assertEquals(
             new HashSet<String>( Arrays.asList( "jdbc", "class", "loop",
-                                                "test", "hapi" ) ),
+                                                "test") ),
                       schemes.keySet() );
         failCreateTable( tfact, ":num:10" );
         tfact.addScheme( new NumTableScheme() );
